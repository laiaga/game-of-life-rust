use std::env;
extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;

use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::{EventSettings, Events};
use piston::input::{RenderArgs, RenderEvent, UpdateArgs, UpdateEvent};
use piston::window::WindowSettings;

pub struct App {
    gl: GlGraphics, // OpenGL drawing backend.
}

impl App {
    fn render(&mut self, args: &RenderArgs) {
        use graphics::*;

        const GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];
        const RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];

        let square = rectangle::square(0.0, 0.0, 50.0);
        let (x, y) = (args.game_board.nb_cols, args.game_board.nb_lines);

        self.gl.draw(args.viewport(), |c, gl| {
            // Clear the screen.
            clear(GREEN, gl);

            let transform = c.transform.trans(x, y).trans(-25.0, -25.0);

            // Draw a box rotating around the middle of the screen.
            rectangle(RED, square, transform, gl);
        });
    }

    fn update(&mut self, args: &UpdateArgs) {
        // Rotate 2 radians per second.
        self.rotation += 2.0 * args.dt;
    }
}

static mut GAME_BOARD: GameBoard = GameBoard {
    nb_cols: -1,
    nb_lines: -1,
};

struct GameBoard {
    nb_cols: i32,
    nb_lines: i32,
}

fn main() {
    parse_arguments();
    println!(
        "Will draw a game of life of {:?} lines per {} columns",
        GAME_BOARD.nb_lines, GAME_BOARD.nb_cols
    );
}

unsafe fn parse_arguments() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        panic!("You need to specify nb of lines and nb of colums as arguments, but {} arguments were received!", args.len()-1);
    }

    let x: &i32 = &args[1].parse::<i32>().unwrap();
    let y: &i32 = &args[2].parse::<i32>().unwrap();

    GAME_BOARD = GameBoard {
        nb_cols: *x,
        nb_lines: *y,
    }
}
