A simple project to learn the basics of Rust

Implement Conaway's Game of Life, see [here](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life). Final goal is to have:
* a GUI
* a list of starting states:
  * manually set the live cells by clicking
  * random distribution
* "stop" and "start" buttons
* speed adjustement
* board size and nb of cells (for the random initial state) configuration

